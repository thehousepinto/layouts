class BooksController < ApplicationController
  layout 'standard'

  def list
    @books = %w(Physics Mathematics Chemistry Psychology Geography)
  end

  def sidebar
    @sidebar = "content for sidebar"
  end
end
